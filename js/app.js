let getAll = document.querySelector("#getAll");
let p = document.querySelector('p');
getAll.onclick = function() {
    if (!window.indexedDB) {
        console.log('This browser does not support IndexedDB');
    }

    var DB = idb.open('New DB using library with promices', 1)
        .then((db) => {
            let transaction = db.transaction('Users collection');
            let store = transaction.objectStore('Users collection');
            // Способ получения всех данных БД
            return store.openCursor()
        })
        .then(function log(cursor) {
            if (!cursor) {
                return
            }
            // cursor.key // первоначальное значение/ключ для элемента
            //console.log(cursor.value); //значения
            for (var field in cursor.value) {
                p.innerHTML += cursor.value[field] + "<br>";
            }
            p.innerHTML += "<hr>"
            return cursor.continue().then(log);

        }).then(() => {
            alert('All objects from DB was taken')
        })
}